# Tour of the Reds

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.0.

## Development server - Angular 4 and Node JS

1. Run `git clone https://rajath_krishna@bitbucket.org/rajath_krishna/tourofthereds_server.git`
2. cd into the cloned directory
3. Run `npm install`
4. Now, open 2 tabs of the terminal / command prompt or 2 terminals to run the angular and node server respectively.
5. Run `ng build --watch` on one tab or terminal and `node app.js` on the other.
6. Navigate to `http://localhost:3000/`. The portal will open up
7. For developmental purposes the following login credentials are used
	`username = admin` & `password = password`

## Angular development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Angular code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Angular build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Angular running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Angular running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help on Angular

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
