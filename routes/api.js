const express = require('express');
const passport = require('passport');
const router = express.Router();
const models = require('../database/models');

const user = models.user;
const beacons = models.beacons;

/**
 * iOS GET API call to fetch the details associated with a beacon
 * @param  {req}      Request object
 * @return {res}      Response object
 */
router.get('/getBeacon', (req, res) => {
    beacons.find({
        where: {
            uuid: req.query.uuid
        }}, {raw: true})
        .then(data => {
            if(!data) {
                return res.status(200).json({info: 'No data available'});
            }
            return res.status(200).json(data);
        })
        .catch(function(err) {
            return res.status(500).json({error: 'Something went wrong while fetching beacon data'});
        });
});

/**
 * GET API call to fetch the details associated with all beacons for the manage beacon portal
 * @param  {req}      Request object
 * @return {res}      Response object
 */
router
router.get('/getBeacons', (req, res) => {
    beacons.findAll({raw: true})
        .then(data => {
            if(!data) {
                return res.status(200).json({info: 'No data available'});
            } 
            return res.status(200).json(data);
        })
        .catch(function(err) {
            return res.status(500).json({error: 'Something went wrong while fetching beacon data'});
        });
});

/**
 * POST API call to add the details of a newly created beacon
 * @param  {req}      Request object
 * @return {res}      Response object
 */
router.post('/addBeacon', (req, res) => {
    beacons.create({uuid: req.body.uuid, beaconName: req.body.name, platform: req.body.platform, message: req.body.message, videoURL: req.body.link})
        .then(data => {
            if(!data) {
                return res.status(200).json({info: 'Some error occured'});
            } 
            return res.status(200).json(data);
        })
        .catch(function(err) {
            return res.status(500).json({error: 'Something went wrong while adding beacon data'});
        });
});

/**
 * UPDATE API call to update the details associated with a beacons 
 * @param  {req}      Request object
 * @return {res}      Response object
 */
router.put('/updateBeacon', (req, res) => {
    beacons.update({
        uuid: req.body.uuid, 
        beaconName: req.body.name, 
        platform: req.body.platform, 
        message: req.body.message, 
        videoURL: req.body.link
    },{where: {
        uuid: req.body.uuid,
        beaconName: req.body.name
    }})
    .then(data=> {
        if(!data){
            return res.status(400).json({'error': 'Beacon already exists'});
        }
        response = { info: 'Success'};
        return res.status(200).json(response);
    })
    .catch(function(err){
        return res.status(500).json({error: 'Something went wrong while adding beacon data'});
    });
});

/**
 * DELETE API call to delete the details of a beacon
 * @param  {req}      Request object
 * @return {res}      Response object
 */
router.delete('/delBeacon', (req, res) => {
    console.log(req);
    beacons.destroy({where: {
        uuid: req.query.uuid,
        beaconName: req.query.name
    }})
    .then(data=> {
        if(!data){
            return res.status(500).json({'error': 'Wrong datae'});
        }
        response = { info: 'Success'};
        return res.status(200).json(response);
    })
    .catch(function(err){
        return res.status(500).json({error: 'Something went wrong while deleting beacon data'});
    });
});

module.exports = router;
