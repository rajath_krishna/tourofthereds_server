import { PCPrepKitPage } from './app.po';

describe('pc-prep-kit App', () => {
  let page: PCPrepKitPage;

  beforeEach(() => {
    page = new PCPrepKitPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
