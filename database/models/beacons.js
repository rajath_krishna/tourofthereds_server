module.exports = function(sequelize, Sequelize) {
 
    const status = sequelize.define('beacons', {
        uuid: {
            type: Sequelize.STRING,
            unique: true 
        },
        beaconName: {
            type: Sequelize.STRING,
            unique: true            
        },
        platform: {
        	type: Sequelize.STRING
        },
        message: {
            type: Sequelize.STRING
        },
        videoURL: {
            type: Sequelize.STRING
        }
    });
    return status;
}
