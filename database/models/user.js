module.exports = function(sequelize, Sequelize) {
 
    const localUser = sequelize.define('user', {
        name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING,
            unique: true
        },
        password: {
            type: Sequelize.STRING              
        },
        resetPasswordToken: {
            type: Sequelize.STRING      
        },
        resetPasswordExpires: {
            type: Sequelize.DATE
        }
    });
    return localUser;
}
