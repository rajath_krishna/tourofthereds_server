import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { environment } from '../environments/environment';
import { APIService } from './api.factory.service';

@Injectable()
export class DashboardService {

	private _addBeacon = environment.baseURL + environment.apiEndpoint + 'addBeacon';
	private _getBeacons = environment.baseURL + environment.apiEndpoint + 'getBeacons';
	private _updateBeacon = environment.baseURL + environment.apiEndpoint + 'updateBeacon';
	private _delBeacon = environment.baseURL + environment.apiEndpoint + 'delBeacon';

 	constructor(private _http: Http, private _apiservice: APIService) { }

  	addBeacon(body: Object): Observable<any> {
    	return this._apiservice.post(this._addBeacon, body)
      	.map(res => res.json());
  	}	

  	updateBeacon(body: Object): Observable<any> {
    	return this._apiservice.put(this._updateBeacon, body)
      	.map(res => res.json());
  	}

  	getBeacons(): Observable<any> {
    	return this._apiservice.get(this._getBeacons)
      	.map(res => res.json());
  	}

  	delBeacon(url: String): Observable<any> {
    	return this._apiservice.del(this._delBeacon + url)
      	.map(res => res.json());
  	}  	  	

}
