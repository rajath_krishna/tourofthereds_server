import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { SplashComponent } from './splash/splash.component';

import { DashboardService } from './dashboard.service';
import { LoginService } from './login.service';
import { LoggedInGuard } from './guards/logged-in.guard';
import { UnAuthGuard } from './guards/unauth.guard';
import { APIService } from './api.factory.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';

// Define the routes
const ROUTES = [
  {
    path: '',
    component: SplashComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [UnAuthGuard]
  },
  {
    path: 'profile',
    component: DashboardComponent
  }
];


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    SplashComponent   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    RouterModule.forRoot(ROUTES) // Add routes to the app
  ],
  providers: [DashboardService, LoginService, LoggedInGuard, UnAuthGuard, APIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
