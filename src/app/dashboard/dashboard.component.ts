import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [DashboardService]
})
export class DashboardComponent implements OnInit {

  uuid: String;
  name: String;
  platform: String;
  text: String;
  link: String;
  data: any;

  settings = {
  columns: {
    uuid: {
      title: 'UUID'
    },
    beaconName: {
      title: 'Beacon name'
    },    
    platform: {
      title: 'Platform'
    },
    message: {
      title: 'Message'
    },    
    videoURL: {
      title: 'Video URL'
    }  
  },
  pager: {
    perPage:50
  },
  edit: {
    editButtonContent: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>',
    confirmSave: true
  },
  delete: {
    deleteButtonContent: '<i class="fa fa-times" aria-hidden="true"></i>',
    confirmDelete: true
  },
  add: {
    addButtonContent: '<i class="fa fa-plus fa-2x" aria-hidden="true"></i>',
    confirmCreate: true
  }
};

  constructor(private loginService: LoginService, private _dashboardService: DashboardService) { }

  ngOnInit() {
    this._dashboardService.getBeacons().subscribe(response => {
      this.data = response;
    }); 
  }

  logout(){
    this.loginService.logout();
  }

  onCreate(event):void { 
    this.uuid = event.newData.uuid;
    this.name = event.newData.beaconName;
    this.platform = event.newData.platform;
    this.text = event.newData.message;
    this.link = event.newData.videoURL;
    const beacon = {
      uuid: this.uuid,
      name: this.name,
      platform: this.platform,
      message: this.text,
      link: this.link
    };
    this._dashboardService.addBeacon(beacon).subscribe(response => {
      event.confirm.resolve(event.newData);
    });
  } 

  onSave(event):void {
    this.uuid = event.newData.uuid;
    this.name = event.newData.beaconName;
    this.platform = event.newData.platform;
    this.text = event.newData.message;
    this.link = event.newData.videoURL; 
    const beacon = {
      uuid: this.uuid,
      name: this.name,
      platform: this.platform,
      message: this.text,
      link: this.link
    };
    this._dashboardService.updateBeacon(beacon).subscribe(response => {
      event.confirm.resolve(event.newData);
    });         
  }

  onDelete(event): void {
    this.uuid = event.data.uuid;
    this.name = event.data.beaconName;
    this._dashboardService.delBeacon('?' + 'uuid=' + this.uuid + '&name=' + this.name).subscribe(response => {
      event.confirm.resolve(event.newData);
    });      
  }

}
