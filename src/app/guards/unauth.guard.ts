import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import { LoginService } from '../login.service';

@Injectable()
export class UnAuthGuard implements CanActivate {

  constructor(private router: Router, private loginService: LoginService) {};

	canActivate(next: ActivatedRouteSnapshot,
	          state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
			if(this.loginService.isLoggedIn()){
				this.router.navigate(['/profile']);
				return false;
			} else {
	            return true;				
			}
	}
	
}


