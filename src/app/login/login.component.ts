import { Component, OnInit } from '@angular/core';
import { NgForm }    from '@angular/forms';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Rx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;
  errorMessage : String;
  pcprepkitlogo: String;

  constructor(private loginService: LoginService, private router: Router, fb: FormBuilder) {
      this.loginForm = fb.group({
        'username' : [null, Validators.required],
        'password' : [null, Validators.compose([Validators.required, Validators.minLength(8)])]
      });
      this.pcprepkitlogo = '../../assets/img/prepkitlogo.png';
   }

  ngOnInit() {
  }
  onSubmit(form: any): void {
	   this.loginService.loginUser(form).subscribe((successful: boolean): void => {
        if (successful) {
          this.router.navigateByUrl('/profile');
        } else {
          this.errorMessage = 'Incorrect username and password, please try again';
        }
	    },err => {
        console.log(err);//get the error in error handler
        this.errorMessage = err.error;
      });   
   }
}
