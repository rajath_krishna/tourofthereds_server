import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class LoginService {

	private static localStorageKey = 'ManUtd';
	private token: string = null; 
	private user: object = null;

	constructor(private http: Http, private router: Router) {
	    const currentUser = JSON.parse(localStorage.getItem(LoginService.localStorageKey));
	    if (currentUser && currentUser.user && currentUser.token) {
	      this.user = currentUser.user;
	      this.token = currentUser.token;
	    } else {
	      localStorage.removeItem(LoginService.localStorageKey);
	    }
	}

	public loginUser (body: Object): Observable<any> {
		this.logout();
	    let bodyString = JSON.stringify(body); 
	    let headers      = new Headers({ 'Content-Type': 'application/json' }); 
	    let options       = new RequestOptions({ headers: headers }); 

	    return this.http.post('http://localhost:3000/auth/login', body, options) // ...using post request
					.map((response:Response) => {
						const json = response.json();
				        if (!json.token) {
				          return false;
				        }
				        localStorage.setItem(LoginService.localStorageKey, JSON.stringify(json.token));
				        this.user = json.user;
				        this.token = json.token;
				        return true;
					})	 
					.do((succeeded: boolean): void => {
						if (succeeded) {
						  this.router.navigateByUrl('/');
						}
					})					 
					.catch((err:Response) => {
						this.logout();
					    let details = err.json();
					    return Observable.throw(details);
					 });              
	}

	public logout() {
		localStorage.removeItem(LoginService.localStorageKey);
		this.user = null;
		this.token = null;
	    if (this.router.url !== '/') {
	      this.router.navigateByUrl('/login');
	    }		
	}	

  	public isLoggedIn() {
  		if(localStorage.getItem(LoginService.localStorageKey)){
  			return true;
  		} else {
  			return false;
  		}
  	}

}
